# Documentation:

Here I will discuss what the technical aspects of what my API does, and specifically, how each module
and code impacts the API, with getting it to return third-party data, which will later on will
be used for my web client.

Firstly, I note down what JavaScript files I need for my API. I know that I need an index.js,
to handle the various modules that I will have, and is the main source of sending requests to 
the third-party API.

I then need to have my film and map API module in seperate files, where it will handle the query strings, 
where what you search form, will communicate with the third-party to return the data about that search.

Then I need a database module, in which handles communications with MongoDB, which later, will allow me to 
add necessary data, get the data from the database, or delete from the database. 

Then finally an authentication module, so I can manipulate this to only store data into MongoDB, if a correct
username and password has been entered, when submitted via **Postman**, which will be touched on later. 

Here is my structure:

```
    +project1
        - authorisation.js
        - films.js
        - index.js
        - map.js
        - mongo.js
        - package.json

```

Now, since index.js is the main source for all communications, I want my modules to be linked up to it. So I want to
create a variable, in which uses methods/functions from the other modules, and use them in index.js. This is how I will
do this:

```
    var films = require('./films.js')
    var mongo = require('./mongo.js')
    var maps = require('./maps.js')
    var authorisation = require('./authorisation.js')
```
This references the file path to each module. 

The referencing is crucial, as now I can use that to help with getting the /GET request to return some data for us using 
a custom search query we search ourselves. 

Before we can do this, we need to create the structure to my URL, so I can get it to search for what I want.
So I create a new module to handle this called **films.js**
So, in this example, I will use my films API.
Here is my base URL structure for this API:

```
    http://www.myapifilms.com/imdb
```
By checking on the official 'myapifilms' API's documentation, by using the query string **title**, it will return the 
film details, based on what title the user has searched for. So this query string **title** will be added to the url like so:

```
    http://www.myapifilms.com/imdb?title
```
Now I want to search for the title of a film, so within the query string again, we see **title** is assigned to **query**.
Query, is what the user puts in, and is assigned to the search term, like below:

```
    http://www.myapifilms.com/imdb?title=Jaws
```
So currently, this is the url string to request data from the third-party API, but this is directly to it. I want it so it gets
passed through my API, in which is requests to the third-party API itself. 
We know the base URL structure above, we can replace that with my own, like so:

```
    https://project1-will12976-2.c9users.io/imdb
```
However, this still won't work, as I didn't define /imdb. In fact the request I'm getting is of /films from the example below:

```
    server.get('/films', (req, res) 
```
So restructuring it once again to this:

```
    https://project1-will12976-2.c9users.io/films
```
Should be our base URL structure.
The rest of the URL string can be the same, as I have kept the parameters the same in films.js

```
    https://project1-will12976-2.c9users.io/films?title=Jaws
```

So, running this in the Postman Rest Client, using my film.js module, should return all the data from the title query we've asked for.
However, there is too much unnecessary data being passed through - stuff we don't want. 
Within the films.js, there is a way of doing this.

```
    request.get({url:url, qs: query_string}, (err, res, body) => 
        {
            if(err){
                callback({code:500, response:{status:'error', message: "Sorry, I am not receiving data at the moment", data:err}})
            }
            console.log(typeof body)
            const json = JSON.parse(body)
            const film = json.map((element) =>{
                return {'title': element.title,'year':element.year, 'genre':element.genres, 'directors': element.directors, 'rated':element.rated, 'time': element.runtime, 'description': element.simplePlot, 'poster': element.urlPoster, 'plot': element.plot, 'rating': element.rating, 'id': element.idIMDB, 'metascore': element.metascore }
            })
            
            console.log(film)
            callback({code:200, response:{status:'success', message: 'Found the Film! '+query, data:film}})
        }
        )
```
We gather that the body that is passed back is our data, and returns in JSON format, so we parse it to turn it into an object.

To filter the type of data that is sent to us, we can use a method called **map**, that takes a function. Here by referencing the data we get back
from Postman, I can take sections of the whole data, by using the parameter of element within that function. You might notice that the way data is passed back is
in dot notation, so it is infact an **array**. Although I am taking back alot of data, I still avoided the kinds of data that I don't want for my API and for my client. 

Then I would pass the data, that I got back into a callback, so that it is returned back into index.js, where I can send a statusCode with it, a message and most
importantly the data that is sent back. 

---------------------
At this point I have explained how third-party data is passed back to my API
---------------------

Now I want to talk briefly, about other functionality that I can include with my /GET requests. Take it for mongoose for instance. Mongoose handles and creates databases,
where we can store data, if it has been requested. So I have installed the mongoose library, and created my own mongo module. This module will handle the various
methods it has, like storing data, deleting them or finding them. 

I start off by making a schema. Schema's are another word for a template. It tells what data type it requires and whether it is a requirement before the various methods run.
I know previously from my films.js, that the data passed back is in fact, an Array. So I create my schema to only allow Arrays, and that it is required before it can do anything
to the database, or it is return an error callback. 

```
    const listSchema = new mongoose.Schema({
        title: { type: Array, required: true }
    })

```

Just to show an example of this working, I would like to use the addList method, so that after a /GET request is sent, if the data sent back is an Array, the data is then passed on
through to the database.

The data is passed through into the addList method, it takes the first index number, as I want to select the whole of the data.
That data is then added to a mongo model, in which checks the schema if its correct, then it will save into mongoDB.

```
exports.addList = (data, callback) => {

    const title = data[0]; 
    const newList = new List({title})
    newList.save((err, data) => {
      if (err) {
        callback('error: '+err)
      }
      callback(data)
    
  })
  
}
```
Now I want to call it, so that when I send my third-party data request, it will run mongo.addList with the data that is passed back.
Storing the film data into a constant called hold, it will take the parameter of mongo.addList, to pass that to MongoDB, where it will
either return a success callback or an error callback.
```
    films.search(searchFilm,(data) => {
        const hold = data.response.data
        mongo.addList(hold, (data) => {
            console.log('/////////////////////////////////////////////////////////////////////////////')
            console.log('Welcome back ' + user + '!')
            console.log('added '+data)
            console.log('Address Details stored in Database')
            console.log('//////////////////////// END OF SEARCH //////////////////////////////////////')
          }) 
    })
    res.setHeader('content-type', 'application/json')
    res.send(data.code, data.response);
    res.end();
```

Since I have commented my code, I won't go into much detail about the other methods. 

Finally I move onto authentication, which requires its own seperate module again. The reason I want an authentication section is to limit
the amount of database entries, when the user sends a /GET request. Because, currently, after each and every /GET request, it will always go
to MongoDB, it can flood the database, sometimes with unnecessary data, so we want to control that, so only authorized users can store their
results into the database.

How this works is, by using the Postman REST Client again, where we pass a username and password field with the request. 
Firstly we need to create an accounts first. We aren't receiving anything, however we are sending our account creation details to the server.
So we will use a **/POST** request. For the purpose of this exercise, I have used the node-persist library, where it stores physical copies of 
each account in a **persist** folder. So we can see it's not as safe as we want it to be. So we will need to use encryption techniques.
In this case I use the **bcrypt** library. This uses something called hashing, where it creates an encryption algorithm, that converts text into
dozens of characters and numbers.

To do this, our authorization module, needs to collect the username and password fields that the user has entered in their own variables.

```
    const username = request.authorization.basic.username
    const password = request.authorization.basic.password
```

Before we get into any encryption or error testing, we need to create an error handler, firstly, incase if no one has typed in the username and password
field, or furthermore, only typed in one field.

```
request.authorization == undefined 
```
We can use this in an 'if' statement, to check if each field is empty or **undefined**. Then we also want to check if the user it using the right scheme that we 
want them to use. In this case, I only want a username and password field, so **Basic Auth** supplies this for me. 

```
request.authorization.scheme != 'Basic'
```

So now we know if the code is still running from here, the user types in a suitable username and password.
But now we need to think, what happens if the username already exists, because we can't have the same username as somebody else.

```
	if (storage.getItemSync(username) !== undefined) {
		callback(new Error('account '+username+' already exists'))
	}
```
Take a look above, here is how we can look for duplicate usernames. So what this does is, it checks the node-persist storage, if the username is 
defined, i.e existant. Then it tells me that a duplicate has been found, where it returns back to index.js a callback with the message that it already
exists. If its equal to undefined, meaning that there isn't a username with that definition, then we know that it doesn't exist in the storage. 

If it goes past these two error handlers, we know that the username and password is correct for creation, and the user is using the correct method of 
authentication.

We now go into the encryption process. **Salt** is used to help generate the hash, which aids the creation of encryption of data.
We then apply this to our password, to create a password hash, which involves letters, numbers basically a load of gibberish.

```
    const salt = bcrypt.genSaltSync(10)
	const hash = bcrypt.hashSync(password, salt)
```

We now attach it to an accounts variable, which takes the username of 'username', and this time, takes hash of 'hash'. So here we have a username, still visible,
however the password has been encrypted, for when we add this to node-persist. To do this we would use **storage.setItem()**.

```
    const hash = bcrypt.hashSync(password, salt)
    const account = {username: username, hash: hash}
    storage.setItem(username, account)
```
Then I want a callback, just to return to the user in Postman, saying that the account has been created.Documentation:


Now, If I want to 'GET' an account from node-persist, I would use the /GET request. By still using Postman, the user types in
a username and password. We check the error handler again, if the user is using **Basic Auth**, and whether they have types anything
in the first place. 
Next, this time I won't be checking if the account is a duplicate of another. This time I will be checking if the username exists on the system, 
because we already know that there aren't any duplicates in the storage, as my addAccount() method helped that. 

```
if (storage.getItemSync(username) === undefined) {
		callback(new Error('account '+username+' does not exist'))
		console.log('Authentification failed - '+username+' does not exist')
	}
```
So here I check if the username is undefined, meaning that it isn't on the system, and it doesn't exist. 

To check if the credentials have been authenticated, we use a comparison of the username and the hashed account.
Checking if the password is wrong with the username, it will return an invalid password error.

```

if (!bcrypt.compareSync(password, account.hash)) {
		callback(new Error('invalid password'))
	}
	
```