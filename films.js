var request = require('request')

//http://www.myapifilms.com/imdb?title=Jaws
//This is the full structure of the third-party API's URL
//In this case, mine will be https://project1-will12976-2.c9users.io/films?title=Jaws

//Remember how this search function was used in index.js?
//By exporting it, I can use it in a different module, if it has been referenced.
exports.search = (query, callback) => {
    
    console.log("Searching, won't take long!")
    //The error handler checks whether the data inputted is not a string, or if its equal to 0.
    //It just checks whether the correct parameters has been entered, or if anything has been added at all, before it moves on.
    if(typeof query !== 'string' || query.length === 0){
        callback({code: 400, response:{status:'error', message:'missing query - enter correct parameters', message1: 'Try entering a postcode'}})
    }
    //This is the base URL to the third-party API, I want to communicate to.
    const url = 'http://www.myapifilms.com/imdb'
    //The query_string is the structure to the rest of the URL, I create it as title, as it is required to return film data of a title query.
    //Then query is equal to whatever has been entered. Finally I have a limit set at 1 for the API testing, however I increase that to 5 for the client.
    const query_string = {title: query, limit: 1}
    console.log(query_string);
    //Now it sends a request to the completed URL structure to the third-party API. It will either return an error or the body, with a response such as the statusCode.
    request.get({url:url, qs: query_string}, (err, res, body) => 
    {
        //If the services become unavailable, it will return the error message, saying that the API is down.
        if(err){
            callback({code:500, response:{status:'error', message: "Sorry, I am not receiving data at the moment", data:err}})
        }
        console.log(typeof body)
        //However if it works successfully, it will parse the body, as it returns as a JSON format.
        const json = JSON.parse(body)
        //Then we map the data received, so I can choose what sections to the body I want. It reduces the amount of unecessary data we dont want/
        const film = json.map((element) =>{
            return {'title': element.title,'year':element.year, 'genre':element.genres, 'directors': element.directors, 'rated':element.rated, 'time': element.runtime, 'description': element.simplePlot, 'poster': element.urlPoster, 'plot': element.plot, 'rating': element.rating, 'id': element.idIMDB, 'metascore': element.metascore }
        })
        console.log(film)
        //Now, I return the callback of the mapped film, and give a success and status code.
        callback({code:200, response:{status:'success', message: 'Found the Film! '+query, data:film}})
    }
    )
}
    