//Requires the encryption library and the node-persist library for storage.
var bcrypt = require('bcrypt')
var storage = require('node-persist')
storage.initSync()

//This function is exported, so I can use this in my index.js page
exports.addAccount = (request, callback) => {

    //This checks whether a username or password has been added
	if (request.authorization == undefined || request.authorization.scheme !== 'Basic') {
		callback(new Error('basic authorization header missing'))
	}
	//Stores the username in its own variable
	const username = request.authorization.basic.username

	//Checks if it already exists
	if (storage.getItemSync(username) !== undefined) {
		callback(new Error('account '+username+' already exists'))
	}
	//Stores the username in its own variable
	const password = request.authorization.basic.password
	/* the 'salt' is the 'cost factor' for the algorithm. */
	const salt = bcrypt.genSaltSync(10)
	//We apply salt to the password hash to become encrypted.
	const hash = bcrypt.hashSync(password, salt)
	//We then store the account details over to node-persist
	const account = {username: username, hash: hash}
	storage.setItem(username, account)
	/* and pass the username back to the routes file. */
	callback(null, {message: 'account created', username: username} )
}

/* this function checks the validity of a supplied username and password. */
exports.getAccount = (request, callback) => {
	console.log("Verifying account with user ...  :  " + request.authorization.basic.username)
	/* does the request include basic authorization. */
	if (request.authorization == undefined || request.authorization.scheme !== 'Basic') {
		callback(new Error('basic authorization header missing'))
	}
	const username = request.authorization.basic.username
	/* we can look up the supplied username to see if the account exists. */
	if (storage.getItemSync(username) === undefined) {
		callback(new Error('account '+username+' does not exist'))
		console.log('Authentification failed - '+username+' does not exist')
	}
	var account = storage.getItemSync(username)
	const password = request.authorization.basic.password
	console.log("Checking if correct password...  :  "+account.hash)
	/* we compare the supplied password to the account hash to see if it is correct. */
	if (!bcrypt.compareSync(password, account.hash)) {
		callback(new Error('invalid password'))
	}
	const data = {username: username}
	callback(null, data)
}


