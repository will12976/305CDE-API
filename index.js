//The libraries used.

var restify = require('restify');
var mongoose = require('mongoose');
var server = restify.createServer({
  name: 'My API',
  version: '4.0.3'
});
server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())


//Here are my links to the various modules - I can call them with their functions using the variable that are assigned to it.
var films = require('./films.js') 
var mongo = require('./mongo.js')
var maps = require('./maps.js')
var authorisation = require('./authorisation.js')


const stdin = process.openStdin()
//Here I can use node.js by typing a statement inside the running of index.js, to use methods from my mongo module
stdin.on('data', (chunk) => {
  console.log(typeof chunk)
  var text = chunk.toString().trim()
  console.log(typeof text)
  //Typing list into node.js retrieves all of the data from mongo.db and displays it in the terminal.
  if (text.indexOf('list') === 0) {
    //It uses the getAll method from mongo.js, to display the full list.
    mongo.getAll( (data) => {
        console.log(data)
    })
  }
  //Typing get then an ID eg(5649ce08d7da945978bc5674), this retrieves the film data with that certain ID.
  if (text.indexOf('get ') === 0) {
    var space = text.indexOf(' ')
    var item = text.substring(space+1).trim()
    console.log('finding: ID "'+item+'"')
    mongo.getById(item, (data) => {
        console.log(data)
    })
  }
  //Typing clear into node.js clears the whole list, so then typing list will give a blank array
  if (text.indexOf('clear') === 0) {
    mongo.clear((data) => {
        console.log(data)
    })
  }
})

server.get('/', (req, res, next) => { //Incase the user doesn't retrieve via /maps, it will redirect
  res.redirect('/films', next)
})


// My first /GET request, here I can request film data back from a third-party api.
server.get('/films', (req, res) => { //Start of getting a request back from GET, and whether it is sucessful or not
  console.log('///////////////////////// NEW SEARCH ////////////////////////////////////////')
  console.log('GET /films')
  //This equals to the query that the user has typed in.
  const searchFilm = req.query.title
  console.log('films='+searchFilm)
  //films is a variable above, linking up to the films.js module. Here I use the search function from films.js
  //It takes the query of the search, then the callback of what it returns.
  films.search(searchFilm,(data) => {
    console.log('/////////////////////////////////////////////////////////////////////////////')
    console.log('///////////////////// AUTHENTICATING ACCOUNT ////////////////////////////')
    const hold = data.response.data//This is the film data returned from films.js
    //Although I could add mongo.addList into my films.js module. The way to have a console.log finishing the storing and searching of the film
    //Is to have it in the index module, so it tells when the search has ended. This is more user-focused. 
    authorisation.getAccount(req, function(err, data) {
      res.setHeader('content-type', 'application/json')

      if (err) {
        res.send(401, {status: 'error', 'server_message': err.message, database_message: 'This film data has NOT been added to the database', data: hold })
        console.log('//////////////////////// END OF SEARCH //////////////////////////////////////')
      } else {
        res.send(200, {status: 'success', message: 'Account Authenticated. Welcome back ' + data.username + '!', database_message: 'This film data has been added to the database!', data: hold})
        const user = data.username;
        mongo.addList(hold, function(data) {
          console.log('/////////////////////////////////////////////////////////////////////////////')
          console.log('Welcome back ' + user + '!')
          console.log('added '+data)
          console.log('Film Details stored in Database')
          console.log('//////////////////////// END OF SEARCH //////////////////////////////////////')
        })        
     }
   })
    res.setHeader('content-type', 'application/json') // The results that come back, will return as JSON text
    res.send(data.code, data.response);
    res.end();
  })
});

server.get('/maps', (req, res) => { //Start of getting a request back from GET, and whether it is sucessful or not
  console.log('///////////////////////// NEW SEARCH ////////////////////////////////////////')
  console.log('GET /maps')
  const searchPlace = req.query.address
  console.log('address='+searchPlace)
  maps.search(searchPlace, (data) => {
    console.log('/////////////////////////////////////////////////////////////////////////////')
    console.log('///////////////////// AUTHENTICATING ACCOUNT ////////////////////////////')
    const hold = data.response.data
    authorisation.getAccount(req, function(err, data) {
      res.setHeader('content-type', 'application/json')

      if (err) {
        res.send(401, {status: 'error', 'server_message': err.message, database_message: 'This film data has NOT been added to the database', data: hold })
        console.log('//////////////////////// END OF SEARCH //////////////////////////////////////')
      } else {
        res.send(200, {status: 'success', message: 'Account Authenticated. Welcome back ' + data.username + '!', database_message: 'This film data has been added to the database!', data: hold})
        const user = data.username;
        mongo.addList(hold, function(data) {
          console.log('/////////////////////////////////////////////////////////////////////////////')
          console.log('Welcome back ' + user + '!')
          console.log('added '+data)
          console.log('Address Details stored in Database')
          console.log('//////////////////////// END OF SEARCH //////////////////////////////////////')
        })        
     }
   })
    res.setHeader('content-type', 'application/json') // The results that come back, will return as JSON text
    res.send(data.code, data.response)
    res.end();
  })
});

/* POST is used to create new accounts. The url points to the 'accounts' collection. */
server.post('/accounts', (req, res) => {
  /* we pass the request object to an async function that takes a callback. The first callback parameter is an Error object or null if no errors are returned. The second paramter is the returned data. */
  authorisation.addAccount(req, (err, data) => {
    /* we will return json data on success or failure */
    res.setHeader('content-type', 'application/json')
    if (err) {
      /* if an error occurs respond with a suitable error code and message. */
      res.send(400, {status: 'error', message: err.message })
    } else {
      /* success! so we return suitable data. */
      res.send(201, {status: 'success', message: 'Account Created!', description: 'Now you can store /GET data into a database!', data: data})
    }
  })
})

var port = process.env.PORT || 8080;
server.listen(port, (err) => {
  if (err) {
      console.error(err);
  } else {
    console.log('App is ready at : ' + port);
  }
})
