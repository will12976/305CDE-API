//Geocoding via Google Maps API http://maps.googleapis.com/maps/api/geocode/output?parameters
//output can be in JSON or XML format, parameters are the various types of data we can collect, in this case, address.

//Final URL structure
//http://maps.googleapis.com/maps/api/geocode/json?address=CV1

//I won't comment all of this, since it is the same structure as my films.js
var request = require('request')

exports.search = (query, callback) => {
    
    console.log("Searching, won't take long!")
    if(typeof query !== 'string' || query.length === 0){
        callback({code: 400, response:{status:'error', message:'missing query - enter correct parameters', message1: 'Try entering a postcode'}})
    }
    const url = 'https://maps.googleapis.com/maps/api/geocode/json'
    const query_string = {address: query}
    console.log(query_string);
    request.get({url:url, qs: query_string}, (err, res, body) => {
        if(err){
            callback({code:500, response:{status:'error', message: "Sorry, I am not receiving data at the moment", data:err}})
        }
        console.log(typeof body)
        const json = JSON.parse(body)
        //The data returned is slightly different to films, as it contains another array in front of the array I want to map. So I need to use dot notation
        //to get to the section to return the data. 
        const map = json.results.map((element) => {
            return {'Address': element.formatted_address, 'Location_Type': element.geometry.location_type,'Lat': element.geometry.location.lat, 'Lng': element.geometry.location.lng, 'Place_ID':element.place_id, 'Type': element.types}
        })
        console.log(map)
        callback({code:200, response:{status:'success', message: 'Found the Address! '+query, data:map}})
    }
    )
}
    